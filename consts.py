import os


TIMERS_PATH = next(
    path
    for path in [
        ".timers.json",
        os.path.join(os.path.expanduser("~"), ".timers.json"),
    ]
    if os.path.isfile(path)
)
