import inspect
import os
import re
from argparse import ArgumentParser
from dataclasses import fields
from datetime import datetime
from io import StringIO
from json import dump, load
from time import sleep
from typing import Generator

from dateutil.parser import parse

from consts import TIMERS_PATH
from timers import TYPES, TimerBase


def from_json(filename: str = TIMERS_PATH) -> Generator[TimerBase, None, None]:
    with open(filename, "r") as fp:
        items = load(fp)

    try:
        for item in items:
            type = TYPES[item["type"]]
            yield type.from_json(item)
    except KeyError:
        raise ValueError(f"Invalid configuration: {item}")


def to_json(timers: list, filename: str = TIMERS_PATH):
    with open(filename, "w") as fp:
        dump(
            [timer.as_json() for timer in timers],
            fp,
            indent=4,
        )


def list_timers(args):
    while True:
        timers = list(from_json())
        strio = StringIO()
        timers_size = len(timers)
        zfill_val = len(str(timers_size))
        for num, timer in enumerate(timers, 1):
            print(
                f"{str(num).zfill(zfill_val)}. {timer.name}: {timer.get_status()}",
                file=strio,
            )

        os.system("cls")
        print(strio.getvalue())
        sleep(args.update_delay)


def edit(args):
    timers = list(from_json())
    for setting in args.settings:
        timer_id, field_name, field_value = re.match(
            r"([0-9]+)\.([a-zA-Z0-9_]+)\=(.*)",
            setting,
            re.I,
        ).groups()
        timer_id = int(timer_id)
        timer = timers[timer_id]
        timer.edit(field_name, field_value)

    to_json(timers)


def add(args):
    timers = list(from_json())
    arg = {field_name: getattr(args, field_name) for field_name in args.fields}
    timer = args.timer_cls(**arg)
    to_json(timers + [timer])


def get_args():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers()
    ls_parser = subparsers.add_parser(
        "ls",
        help="view timers",
    )
    ls_parser.add_argument(
        "-u",
        "--update-delay",
        type=lambda x: int(x) if x else 1,
        default=1,
    )
    ls_parser.set_defaults(func=list_timers)

    edit_parser = subparsers.add_parser(
        "set",
        help="change timer setting or settings (format <task index>.<field_name>=field_value)",
    )
    edit_parser.add_argument("settings", nargs="+")
    edit_parser.set_defaults(func=edit)

    add_parser = subparsers.add_parser(
        "add",
        help="add new timer",
    )
    add_subparsers = add_parser.add_subparsers()
    for type_name, type_ in TYPES.items():
        another_parser = add_subparsers.add_parser(
            type_name,
            help=f"create a {type_name} timer",
        )
        parnames = [
            par for par in inspect.signature(type_.__init__).parameters if par != "self"
        ]
        parnames_to_fields = {par: field for par, field in zip(parnames, fields(type_))}
        for parname, field in parnames_to_fields.items():
            if field.type == datetime:
                another_parser.add_argument(parname, type=parse)
            else:
                another_parser.add_argument(parname)

        another_parser.set_defaults(
            timer_cls=type_,
            type_name=type_name,
            fields=parnames,
        )

    add_parser.set_defaults(func=add)
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    try:
        args.func(args)
    except KeyboardInterrupt:
        os.system("cls")
        exit()
