from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from typing import ClassVar

import human_readable
from dateutil.parser import parse


@dataclass
class TimerBase(ABC):
    name: str
    type: ClassVar[str]

    @abstractmethod
    def get_status(self) -> str:
        ...

    @classmethod
    @abstractmethod
    def from_json(cls, obj: dict) -> "TimerBase":
        ...

    def as_json(self) -> dict:
        return {
            "name": self.name,
            "type": self.type,
        }

    @staticmethod
    def format_timedelta(td):
        return human_readable.precise_delta(
            td,
            minimum_unit="seconds",
            formatting="0.0f",
        )

    def edit(self, field_name, field_value):
        setattr(self, field_name, field_value)


@dataclass
class CountdownTimer(TimerBase):
    end_time: datetime
    type: ClassVar[str] = "count-down"

    def as_json(self) -> dict:
        return {
            **super().as_json(),
            "end_time": self.end_time.isoformat(),
        }

    @classmethod
    def from_json(cls, obj: dict) -> TimerBase:
        return cls(
            name=obj["name"],
            end_time=datetime.fromisoformat(obj["end_time"]),
        )

    def edit(self, field_name, field_value):
        if field_name == "end_time":
            self.end_time = parse(field_value)
        else:
            super().edit(field_name, field_value)

    def get_status(self):
        now = datetime.now()
        if self.end_time < now:
            return "finished"
        return "%s left" % self.format_timedelta(self.end_time - datetime.now())


@dataclass
class CountUpTimer(TimerBase):
    start_time: datetime
    type: ClassVar[str] = "count-up"

    def as_json(self) -> dict:
        return {
            **super().as_json(),
            "start_time": self.start_time.isoformat(),
        }

    @classmethod
    def from_json(cls, obj: dict) -> TimerBase:
        return cls(
            name=obj["name"],
            start_time=datetime.fromisoformat(obj["start_time"]),
        )

    def get_status(self):
        now = datetime.now()
        if self.start_time > now:
            return "not started yet"
        return "%s passed" % self.format_timedelta(now - self.start_time)

    def edit(self, field_name, field_value):
        if field_name == "start_time":
            self.start_time = parse(field_value)
        else:
            super().edit(field_name, field_value)


TYPES: dict[str, type[TimerBase]] = {
    c.type: c
    for c in (
        CountdownTimer,
        CountUpTimer,
    )
}
